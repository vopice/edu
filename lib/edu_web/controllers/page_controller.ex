defmodule EduWeb.PageController do
  use EduWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
