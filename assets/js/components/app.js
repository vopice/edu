import React from 'react';
import Dashboard from './dashboard';

const App = () => (
  <div>
    <Dashboard />
  </div>
)
export default App
