import User from './user'

const combinedReducers = {
  user: User
};

export default combinedReducers;
